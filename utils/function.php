<?php
/**
 * cleanFolderUpload
 *
 *
 * @return void
 * @since  2019-09-09
 */
function cleanFolderUpload()
{
    $current_date=date('Ymd');
    if ($file_list=opendir("storage/upload/")) {
        while (($file=readdir($file_list)) !== false) {
            if (!in_array($file, array(
                    ".",
                    "..",
                    $current_date
                )) && is_dir("storage/upload/" . $file)) {
                deleteDir("storage/upload/" . $file);
            }
        }
    }

}

/**
 * write_log
 *
 * @param $content
 *
 * @return void
 * @author quoc_thinh
 * @since  2019-09-09
 */
function write_log($content)
{
    $current_date=date("Ydm");
    if (!file_exists("logs/" . $current_date)) {
        @mkdir("logs/" . $current_date, 0777, true);
        @chmod("logs/" . $current_date, 0777);
    }

    $fpath  ="logs/" . $current_date . "/log.txt";
    $fp     =fopen($fpath, 'a+');
    $content=date("Y-m-d H:i:s") . " - " . $content . "\r\n";
    fwrite($fp, $content);
    fclose($fp);
}

/**
 * deleteDir
 *
 * @param $dirPath
 *
 * @return void
 * @author quoc_thinh
 * @since  2019-09-09
 */
function deleteDir($dirPath)
{
    if (!is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath.='/';
    }
    $files=glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

/**
 * create_zip_file_to_download
 *
 * @param string $file_path
 * @param string $file_name
 *
 * @return void
 * @since  2019-09-09
 */
function create_zip_file_to_download($file_path='', $file_name='test')
{
    $rootPath=realpath($file_path);

// Initialize archive object
    $zip=new ZipArchive();
    $zip->open($file_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files=new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name=>$file) {
        // Skip directories (they would be added automatically)
        if (!$file->isDir()) {
            // Get real and relative path for current file
            $filePath    =$file->getRealPath();
            $relativePath=substr($filePath, strlen($rootPath) + 1);

            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }

// Zip archive will be created only after closing object
    $zip->close();
}

/**
 * set_message
 *
 * @param $code
 * @param $message
 *
 * @return array
 * @author quoc_thinh
 * @since  2019-09-09
 */
function set_message($code, $message){
    return [
        'code'      => $code,
        'message'   => $message
    ];
}

/**
 * data_new_line_to_string
 *
 * @param $string
 *
 * @return mixed
 * @author quoc_thinh
 * @since  2019-09-09
 */
function data_new_line_to_string($string){
    $string = str_replace(array("\n", "\r"), array("\\n", "\\r"), $string);
    return $string;
}

/**
 * data_string_to_new_line
 *
 * @param      $string
 * @param bool $is_html
 *
 * @return mixed
 * @author quoc_thinh
 * @since  2019-09-09
 */
function data_string_to_new_line($string, $is_html = false){
    if ($is_html){
        $string = str_replace(array("\\n", "\\r"), array("<br>", "<br>"), $string);
    }else{
        $string = str_replace(array("\\n", "\\r"), array("\n", "\r"), $string);
    }
    return $string;
}

/**
 * get_columns_from_table
 *
 * @param      $connect_real
 * @param      $table
 * @param bool $remove_id
 *
 * @return array
 * @author quoc_thinh
 * @since  2019-09-09
 */
function get_columns_from_table($connect_real, $table, $remove_id = true)
{
    $columns = "DESCRIBE $table";
    $result = mysqli_query($connect_real, $columns);
    $col = array();
    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $col[] = $row['Field'];
        }
    }
    if ($remove_id) {
        unset($col[0]);
    }
    return $col;
}
