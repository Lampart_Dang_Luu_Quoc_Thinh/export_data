<?php
error_reporting(E_ERROR | E_PARSE);

include ('config/table.php');
include ('config/database.php');
include ('utils/function.php');

cleanFolderUpload();

$result = [
    'code'      => '',
    'message'   => ''
];

// Get info server dev to import
$server_real = $config_database['dev_real'];
if (!empty($_POST['hostnameExport'])
    || !empty($_POST['usernameExport'])
    || !empty($_POST['passwordExport'])
    || !empty($_POST['databaseExport']))
{
    $server_real = [
        'hostname' => $_POST['hostnameExport'],
        'username' => $_POST['usernameExport'],
        'password' => $_POST['passwordExport'],
        'database' => $_POST['databaseExport']
    ];
}


$server_dev  = $config_database['dev_local'];
if (!empty($_POST['hostname'])
    || !empty($_POST['username'])
    || !empty($_POST['password'])
    || !empty($_POST['database']))
{
    $server_dev = [
        'hostname' => $_POST['hostname'],
        'username' => $_POST['username'],
        'password' => $_POST['password'],
        'database' => $_POST['database']
    ];
}

// Create connection
$connect_real = mysqli_connect(
    $server_real['hostname'],
    $server_real['username'],
    $server_real['password'],
    $server_real['database']
);
mysqli_set_charset($connect_real,"utf8");

$connect_dev = mysqli_connect(
    $server_dev['hostname'],
    $server_dev['username'],
    $server_dev['password'],
    $server_dev['database']
);
mysqli_set_charset($connect_dev,"utf8");

try {
    // ========================================Start progress export
    if (isset($_POST['btn_export_data_csv'])){

        if (!$customer_id_post = $_POST['customer_id']) {
            throw new Exception("Input Customer ID please!!!");
        }

        if (!$connect_real ) {
            throw new Exception("Connection Database Export Error");
        }
        $foldername             =   date('YmdHis');
        $config_export_data_csv =   $config_table['export_data_csv'];

        if (!file_exists("storage/download/". $foldername)) {
            @mkdir("storage/download/". $foldername, 0777, true);
            @chmod("storage/download/". $foldername, 0777);
        }
        $id_list    = array();
        $flag_error = false;
        $customer_list = explode(',', $customer_id_post);
        // Export CSV
        foreach ($customer_list as $customer_id){
            $customer_id    = trim($customer_id);
            $current_date   = date('YmdHis');
            $fpath = "storage/download/". $foldername . "/" . $customer_id . "_" . $current_date . '_data.csv';
            $fp = fopen($fpath, 'w+');

            if (!$export_result = export_data_csv($connect_real, $config_export_data_csv, $id_list, $customer_id)){
                $result = set_message('customer_id_not_found', 'Customer ID "' . $customer_id . '" not exist');
                $flag_error = true;
                fclose($fp);
                deleteDir("storage/download/". $foldername);
                break;
            }

            fclose($fp);
        }

        // Create zip file and download CSV
        if (!$flag_error) {
            $zip = new ZipArchive();
            create_zip_file_to_download("storage/download/". $foldername, "storage/download/". $foldername . '.zip');

            header("Content-type: application/zip");
            header("Content-Disposition: attachment; filename=" .$foldername . ".zip");
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile("storage/download/". $foldername . ".zip");
            deleteDir("storage/download/". $foldername);
            exit;
        }
    }// ======================================== end export data from real

    // Progress import data
    if (isset($_POST['btn_import_data_csv']))
    {
        if (!$connect_dev) {
            throw new Exception("Connection Database Dev Import Error");
        }

        //Check file format and exist
        if (empty($_FILES['csv_import']['name'])) {
            $result = set_message('file_error', 'Cannot find File Import!');
            throw new Exception("File is not exist");
        }
        $accepted_types = [
            'application/zip',
            'application/x-zip-compressed',
            'multipart/x-zip',
            'application/s-compressed'
        ];
        if(!in_array($_FILES['csv_import']['type'], $accepted_types)){
            $result = set_message('type error', 'Error format file');
            throw new Exception("Error format file");
        }

        $table          =   '';
        $data           =   [];
        $data_item      =   [];
        $field_list     =   [];
        $map_id_list    =   [];

        $config_export_data_csv =   $config_table['export_data_csv'];
        $filename               =   $_FILES['csv_import']['tmp_name'];

        $zip = new ZipArchive;
        $res = $zip->open($filename);

        if ($res !== TRUE) {
            $result = set_message('file_error', 'Cannot open File Import!');
            throw new Exception("Cannot open File Import!");
        }

        $current_date       =   date('Ymd');
        $current_datetime   =   date('YmdHis');
        $folder_upload      =   "storage/upload/". $current_date ."/temp_". $current_datetime . "/";

        if (!file_exists($folder_upload)) {
            @mkdir($folder_upload, 0777, true);
            @chmod($folder_upload, 0777);
        }

        $zip->extractTo($folder_upload);
        $zip->close();
        // Start read file
        if ($file_list = opendir($folder_upload)) {
            $viewResult = array();
            $have_file  = false;

            while (($file = readdir($file_list)) !== false) {
                $pathinfo = pathinfo($file);

                if ($file != "." && $file != ".." && $pathinfo['extension'] == 'csv') {
                    $have_file      =   true;
                    $filename       =   $folder_upload . $file;
                    $handle         =   fopen($filename, "rb");
                    $contents       =   fread($handle, filesize($filename));
                    $array_content  =   preg_split("/((\r?\n)|(\r\n?))/", $contents);

                    foreach($array_content as $line){

                        if (substr($line, 0, 13) == START_TABLE){
                            $table                  =   substr($line,13);
                            $data[$table]           =   [];
                            $map_id_list[$table]    =   [];
                            $field_list[$table]     =   [];
                        }else if (substr($line, 0, 11) == END_TABLE || $line == ''){
                            continue;
                        }else{
                            $line       = substr($line, 1,  -1);
                            $items      = explode('","', $line);
                            $count_item = count($items);

                            if (substr($items[0], 0) == 'id'){
                                foreach ($items as $item){
                                    // Change column data in new table java structure
                                    if (array_key_exists($item, $keyTransfer)) {
                                        $item = $keyTransfer[$item];
                                    }
                                    array_push($field_list[$table], $item);
                                }
                            }else{
                                for ($i = 0; $i < $count_item; $i++){
                                    //convert string to import and show table html
                                    if (isset($_POST['btn_import_data_csv'])){
                                        $items[$i] = data_string_to_new_line($items[$i]);
                                    }else{
                                        $items[$i] = data_string_to_new_line($items[$i], true);
                                    }
                                    $data_item += array(
                                        $field_list[$table][$i] => $items[$i]
                                    );

                                }
                                array_push($data[$table],$data_item );
                                $map_id_list[$table]+= array(
                                    $data_item['id'] => ''
                                );
                                $data_item          = array();
                            }
                        }
                    }
                    // Start insert to database
                    try{
                        mysqli_query($connect_dev, "START TRANSACTION");
                        if ($result =  import_data_csv($connect_dev, $config_export_data_csv, $field_list, $data, $map_id_list)){
                            $result = set_message('import_success', "Import file success");
                        }else{
                            throw new Exception("ERROR IMPORT DATABASE");
                        }
                        mysqli_query($connect_dev, "COMMIT");
                        write_log( __FILE__ . " - " . __LINE__ . " - " . " Import Success!");
                    }catch (Exception $e){
                        $result = set_message('import_error', "Error while importing data with file: ". $pathinfo['basename'] ."!");
                        mysqli_query($connect_dev, "ROLLBACK");
                        write_log( __FILE__ . " - " . __LINE__ . " - " . " Import Failed!");
                        break;
                    }
                    fclose($handle);
                }
            }

            closedir($file_list);

            if (!$have_file){
                write_log( __FILE__ . " - " . __LINE__ . " - " . "File Empty");
            }
        }
    }
} catch (Exception $ex) {
    $result = set_message('server_error',  $ex->getMessage());
    write_log( __FILE__ . " - " . __LINE__ . " - " . $ex->getMessage());
}
require ('index.php');

/**
 * export_data_csv
 *
 * @param array  $connect_real
 * @param array  $config
 * @param array  $id_list
 * @param string $customer_id
 *
 * @return bool
 * @since  2019-09-09
 */
function export_data_csv($connect_real, $config, $id_list = array(), $customer_id = ''){
    $check_data = false;
    global $fp;

    foreach ($config as $key => $value){
        $data = '';
        set_header($connect_real, $key);
        $sql = "SELECT ". $key . '.* FROM ' . $key;
        $sql .= " WHERE 1 = 1 ";
        if ($customer_id != ''){
            $sql .= "AND " . $key . ".id = " . $customer_id;
        }else if (!empty($value['column'])){
            $numItem = count($value['column']);
            $i = 0;
            foreach ($value['column'] as $key_column => $value_column){
               if (!empty($id_list[$key_column])){
                   if ($i == 0){
                       $sql .= " AND ";
                   }else if($i != $numItem){
                       $sql .= " OR ";
                   }
                   $sql .= " ". $key . "." . $value_column . " IN (" . implode(", ", $id_list[$key_column]) . ") ";
                   $i++;
               }else{
                   $sql .= "AND true = false";
               }
            }
        }

        if (!empty($value['condition'])){
            foreach ($value['condition'] as $key_condition => $value_condition){
                $sql .= "AND ". $key. "." . $key_condition . " = " . $value_condition . " ";
            }
        }
        $result = mysqli_query($connect_real, $sql);
        if ($result){
            while ($row = mysqli_fetch_assoc($result)){
                foreach ($row as &$item_row){
                    $item_row = data_new_line_to_string($item_row);
                }
                $check_data = true;
                $data .= '"';
                $data .= implode('","', $row);
                $data .= '"';
                $data .= "\r\n";

                if (isset($id_list[$key])){
                    if (!in_array( $row['id'], $id_list[$key])){
                        array_push($id_list[$key], $row['id']);
                    }
                }else{
                    $id_list[$key] = array(
                        $row['id']
                    );
                }
            }
        }

        if ($customer_id != '' && $check_data == false){
            return false;
        }

        if (!empty($data)){
            fwrite($fp, $data);
        }
        set_footer($key);
        write_log( __FILE__ . " - " . __LINE__ . " - " . " Create data for table: ". $key);
        if (!empty($value['table_related'])){
            export_data_csv($connect_real, $value['table_related'], $id_list);
        }
    }
    return true;
}

/**
 * set_header
 *
 * @param $connect_real
 * @param $table_name
 *
 * @return void
 * @since  2019-09-09
 */
function set_header($connect_real, $table_name){
    global $fp;
    $column_name = get_columns_from_table($connect_real, $table_name, false);
    if (!empty($column_name)){
        $header = "start_table: " . $table_name . "\r\n";
        $header .= "\"";
        $header .= implode("\",\"", $column_name);
        $header .= "\"\r\n";
        fwrite($fp, $header);
    }
}

/**
 * set_footer
 *
 * @param $table_name
 *
 * @return void
 * @since  2019-09-09
 */
function set_footer($table_name){
    global $fp;
    $footer = "end_table: " . $table_name . "\r\n";
    $footer .= "\r\n";
    fwrite($fp, $footer);
}

/**
 * import_data_csv
 *
 * @param array $connect_dev
 * @param array $config
 * @param array $field_data
 * @param array $data
 * @param array $map_id_list
 *
 * @return bool
 * @since  2019-09-09
 */
function import_data_csv($connect_dev, $config, $field_data = array(), $data = array(), &$map_id_list = array()){
    try{

        foreach ($config as $table => $value_config){
            $foregn_key = array();
            if (!empty($value_config['column'])){
                $foregn_key = $value_config['column'];
            }

            if (!insert_and_map_id($connect_dev, $table, $field_data[$table], $data[$table], $map_id_list, $foregn_key)){
                write_log( "ERROR when insert ". $table);

                throw new Exception("ERROR WHEN INSERT DATABASE");
            }
            if (!empty($value_config['table_related'])){
                if (!import_data_csv($connect_dev, $value_config['table_related'], $field_data, $data, $map_id_list)){
                    throw new Exception("ERROR WHEN IMPORT");
                }
            }
        }
        return true;
    }catch (Exception $exception){
        return false;
    }
}

/**
 * insert_and_map_id
 *
 * @param array $connect_dev
 * @param array $table
 * @param array $field
 * @param array $data
 * @param array $map_id
 * @param array $foregn_key
 *
 * @return bool
 * @since  2019-09-09
 */
function insert_and_map_id($connect_dev, $table, $field = [], $data = [], &$map_id, $foregn_key = []){

    if (!empty($data)){
        unset($field[0]);
        $column =  '`';
        $column .=  implode("`, `", $field);
        $column .=  '`';
        $insert = 'INSERT INTO ' . $table . ' ('. $column .') ';
        $insert .= 'VALUES ';
        foreach ($data as $item){
            $old_id = $item['id'];
            if (!empty($foregn_key)){
                foreach ($foregn_key as $key_foregn_key => $value_foregn_key){
                    $item[$value_foregn_key] = $map_id[$key_foregn_key][$item[$value_foregn_key]];
                }
            }
            unset($item['id']);
            $value = "'";
            $value .=  implode("', '", $item);
            $value .= "'";
            $sql = $insert .'(' . $value . ')';
            $result = mysqli_query($connect_dev, $sql);

            if ($result){
                $last_id = mysqli_insert_id($connect_dev);
                $map_id[$table][$old_id] = $last_id;
            } else{
               write_log( "ERROR when insert ". $table);
                return false;
           }
        }
    }
    return true;
}
