<?php
// DB Info default
$config_database = [
   'dev_local' => [
       'hostname' => '172.16.217.22',
       'username' => 'root',
       'password' => 'lampart',
       'database' => 'pwater_java_final',
   ],
   'dev_real' => [
       'hostname' => '172.16.100.14',
       'username' => 'pwater',
       'password' => 'pwater',
       'database' => 'pwater_java',
   ]
];

// Key replace name
$keyTransfer = [
    'lastup_account_id' => 'updated_by',
    'lastup_datetime'   => 'updated_at',
    'create_datetime'   => 'created_at',
    'disable'           => 'is_deleted',
];

// Constant declare
CONST START_TABLE   = "start_table: ";
CONST END_TABLE     = "end_table: ";
